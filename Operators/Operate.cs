﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operators
{
    public class Operate
    {
        public int add(int a, int b)
        {
            return a + b;
        }

        public int substract(int a, int b)
        {
            return a - b;
        }

        public int multiply(int a, int b)
        {
            return a * b;
        }

        public decimal divide(int a, int b)
        {
            return (decimal)a / (decimal)b;
        }
        public void nothing()
        {
        }
    }
}
